#! /bin/bash

set -e
set -u

VERSION=3.0.1
SHA1HASH=9851f9eebea8d98511a56387e72f4c67ac28399f

upstream=alberta-${VERSION}.tar.xz
debian=alberta_${VERSION}.orig.tar.xz
url=http://www.mathematik.uni-stuttgart.de/fak8/ians/lehrstuhl/nmh/downloads/alberta/${upstream}

if [[ -f ${debian} ]]; then
  echo "${debian} already exists." >&2
  exit 1
fi

if ! [[ -f ${upstream} ]]; then
  wget -O ${upstream} ${url}
fi

sha1hash=$(sha1sum ${upstream})
sha1hash=${sha1hash%% *}

if [[ ${sha1hash} != ${SHA1HASH} ]]; then
  echo "SHA1 hash differs from recorded value." >&2
  exit 1
fi

mkdir alberta-${VERSION}.orig
cd alberta-${VERSION}.orig

tar --strip-components=1 -xf ../${upstream}

find . -path ./demo -prune -o -name Makefile.in -exec rm "{}" +
rm aclocal.m4 configure config.guess config.sub
rm compile depcomp install-sh missing
rm ltmain.sh m4/libtool.m4 m4/ltoptions.m4 m4/ltsugar.m4 m4/ltversion.m4 m4/lt~obsolete.m4

cd ..
tar --owner=root --group=root -c alberta-${VERSION}.orig | xz -9 > ${debian}
